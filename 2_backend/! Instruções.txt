
Esta avalia��o trata das seguintes tecnologias e arquiteturas:
- DDD
- Dapper
- MsSql
- Asp.net MVC

1. Crie uma nova base de dados
2. Restaure o script instnwnd.sql, localizado na pasta Avalia��o
3. Seguindo os padr�es da arquitetura DDD e a estrutura dos projetos:
	3.1 Crie um contexto para o novo banco de dados, usando Dapper e mapeie apenas a tabela Region
	3.2	Crie um reposit�rio para a tabela Region, com as a��es: Listar todos, Detalhes(id), Incluir, Atualizar(id) e Excluir(id)
	3.3 Crie um servi�o para as opera��es do reposit�rio Region
4. Na camada de aplica��o, crie um controller capaz de realizar todas as a��es do servi�o Region
5. Execute cada um dos m�todos criados no controller atr�ves do Swagger (http://{url}/swagger/ui/index)

